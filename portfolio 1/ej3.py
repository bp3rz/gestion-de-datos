import mysql.connector
import sys

def printHelp():
    print('''
Usage: python3 users.py <command>
Available commands:
    - addUser <name> <surname> <email> <password>
    - listUser [<query>]
    - updateUser <email> <att>=<val>{,<att>=<val>}
    - rmUser <email>
    - addFriend <ownerEmail> <targetEmail>
    - rmFriend <ownerEmail> <targetEmail>
    - lsFriends <email>
''')

def init():
    con = mysql.connector.connect(user="root", password="root")
    try:
        cur = con.cursor()
        cur.execute("CREATE DATABASE IF NOT EXISTS myusers")
        cur.execute("USE myusers")
        cur.execute('''
            CREATE TABLE IF NOT EXISTS User (
                name CHAR(32) NOT NULL,
                surname CHAR(32) NOT NULL,
                email CHAR(64),
                password CHAR(64) ,
                PRIMARY KEY(email)
            );''')
        print("tabla Users creada")
        cur.execute('''
            CREATE TABLE IF NOT EXISTS Friend (
                owner CHAR(64) ,
                target CHAR(64),
                FOREIGN KEY (owner) REFERENCES User(email) ON DELETE CASCADE,
                FOREIGN KEY (target) REFERENCES User(email) ON DELETE CASCADE
            );''')
        print("tabla Friend creada")
        con.commit()
        print("Database created.")
    except Exception as e:
        print("Error during initialization:", e)
    finally:
        cur.close()
        con.close()
init()




def addUser(name, surname, email, password):
    con = mysql.connector.connect(user="root", password="root", database="myusers")
    try:
        cur = con.cursor()
        cur.execute("INSERT INTO User (name, surname, email, password) VALUES (%s, %s, %s, %s)", (name, surname, email, password))
        con.commit()
        print("User added successfully.")
    except Exception as e:
        print("Error adding user:", e)
    finally:
        if con.is_connected():
            con.close()
            
            
            
            
            

def listUsers(query=""):
    print("users listing:")
    con = mysql.connector.connect(user="root", password="root", database="myusers")
    users = []
    try:
        cur = con.cursor()
        if query:
            cur.execute("SELECT * FROM User WHERE name LIKE %s OR surname LIKE %s OR email LIKE %s", ('%' + query + '%', '%' + query + '%', '%' + query + '%'))
        else:
            cur.execute("SELECT * FROM User")

        for row in cur.fetchall():
            user = {
                
                "name": row[0],
                "surname": row[1],
                "email": row[2],
                "password": row[3]
            }
            users.append(user)
    except Exception as e:
        print("Error listing users:", e)
    finally:
        if con.is_connected():
            con.close()
    return users
    
    
    
    
    

def updateUser(email, data={}):
    print("update user: ",email)
    con = mysql.connector.connect(user="root", password="root", database="myusers")
    try:
        cur = con.cursor()
        update_query = "UPDATE User SET "
        for key, value in data.items():
            update_query += f"{key} = %s, "
        update_query = update_query[:-2]  
        update_query += " WHERE email = %s"
        cur.execute(update_query, list(data.values()) + [email])
        con.commit()
        print("User updated successfully.")
    except Exception as e:
        print("Error updating user:", e)
    finally:
        if con.is_connected():
            con.close()
            
            
            
            
            

def removeUser(email):
    con = mysql.connector.connect(user="root", password="root", database="myusers")
    try:
        cur = con.cursor()
        cur.execute("DELETE FROM User WHERE email = %s", (email,))
        con.commit()
        print("User removed successfully.")
    except Exception as e:
        print("Error removing user:", e)
    finally:
        if con.is_connected():
            con.close()
            
            
            
            
def addFriend(ownerEmail, targetEmail):
    con = mysql.connector.connect(user="root", password="root", database="myusers")
    try:
        cur = con.cursor()
        cur.execute("SELECT email FROM User WHERE email IN (%s, %s)", (ownerEmail, targetEmail))
        results = cur.fetchall()
        if len(results) == 2:  
            cur.execute("INSERT INTO Friend (owner, target) VALUES (%s, %s)", (ownerEmail, targetEmail))
            con.commit() 
            print("Friends added")
        else:
            print("One or both emails not found")
    except mysql.connector.Error as err:
        print(f"Error: {err}")
    finally:
        cur.close() 


def listFriend(email):
    con = mysql.connector.connect(user='root', password='root', database='myusers')
    try:
        cur = con.cursor()
        cur.execute("SELECT target FROM Friend WHERE owner = %s", (email,))
        friends = cur.fetchall()
        if friends:
            print("Friends of", email, ":")
            for friend in friends:
                print(friend[0])
        else:
            print("No friends found for this user.")
    except Exception as e:
        print(f"Error listing friends: {e}")
    finally:
        if con.is_connected():
            con.close()
            
            
            
            
            

def removeFriend(ownerEmail,targetEmail):
    con = mysql.connector.connect(user='root', password='root', database='myusers')
    try:
        cur = con.cursor()
        cur.execute("DELETE FROM Friend WHERE owner = %s AND target = %s", (ownerEmail, targetEmail))
        con.commit()
        print("Friendship removed successfully.")
    except Exception as e:
        print(f"Error removing friendship: {e}")
    finally:
        if con.is_connected():
            con.close()
            
            
            

if len(sys.argv) < 2:
    printHelp()
elif sys.argv[1] == "addUser":
    addUser(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
elif sys.argv[1] == "listUser":
    if len(sys.argv)<=2: users=listUsers()
        
    else:users=listUsers(sys.argv[2])
    for user in users:
        print(f"- name:{user['name']}  surname:{user['surname']}  email:{user['email']}  password:{user['password']} ")
elif sys.argv[1] == "updateUser":
            if len(sys.argv) >= 4:
                email = sys.argv[2]
                data = {}
                for arg in sys.argv[3:]:
                    key_value = arg.split('=')
                    if len(key_value) == 2:
                        data[key_value[0]] = key_value[1]
                updateUser(email, data)
            else:
                print("Uso: users.py update <email> <attr>=<val>")

elif sys.argv[1] == "rmUser":
    removeUser(sys.argv[2])
elif sys.argv[1] == "addFriend":
    addFriend(sys.argv[2], sys.argv[3])
elif sys.argv[1] == "lsFriends":
    listFriend(sys.argv[2])
    
elif sys.argv[1] == "rmFriend":
    removeFriend(sys.argv[2])   
else:
    printHelp()
