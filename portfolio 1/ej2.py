import mysql.connector
import sys

def printHelp():
    print('''
Usage: python3 users.py <command>
Available commands:
    - add <name> <surname> <email> <password>
    - ls [<query>]
    - update <email> <att>=<val>{,<att>=<val>}
    - rm <email>
''')




def init():
    con = mysql.connector.connect(host="localhost", user="root", password="root", database="myusers")
    try:
        cur = con.cursor()
        cur.execute("CREATE DATABASE IF NOT EXISTS myusers")
        cur.execute("USE myusers")
        cur.execute('''CREATE TABLE IF NOT EXISTS User (
                            id INT AUTO_INCREMENT PRIMARY KEY,
                            name CHAR(32),
                            surname CHAR(32),
                            email CHAR(64),
                            password CHAR(64)
                        )''')
        con.commit()
        print("Database created.")
    except Exception as e:
        #print(e)
        print("Database already exists.")
    finally:
        if con.is_connected():
            con.close()
init()





def addUser(name, surname, email, password):
    con = mysql.connector.connect(user="root", password="root", database="myusers")
    try:
        cur = con.cursor()
        cur.execute("INSERT INTO User (name, surname, email, password) VALUES (%s, %s, %s, %s)", (name, surname, email, password))
        con.commit()
        print("User added successfully.")
    except Exception as e:
        print("Error adding user:", e)
    finally:
        if con.is_connected():
            con.close()
            
            
            
            

def listUsers(query=""):
    con = mysql.connector.connect(user="root", password="root", database="myusers")
    users = []
    try:
        cur = con.cursor()
        if query:
            cur.execute("SELECT * FROM User WHERE name LIKE %s OR surname LIKE %s OR email LIKE %s", ('%' + query + '%', '%' + query + '%', '%' + query + '%'))
        else:
            cur.execute("SELECT * FROM User")
        for row in cur.fetchall():
            user = {
                "id": row[0],
                "name": row[1],
                "surname": row[2],
                "email": row[3],
                "password": row[4]
            }
            users.append(user)
    except Exception as e:
        print("Error listing users:", e)
    finally:
        if con.is_connected():
            con.close()
    return users
    
    
    

def updateUser(email, data={}):
    con = mysql.connector.connect(user="root", password="root", database="myusers")
    try:
        cur = con.cursor()
        update_query = "UPDATE User SET "
        for key, value in data.items():
            update_query += f"{key} = %s, "
        update_query = update_query[:-2]  
        update_query += " WHERE email = %s"
        cur.execute(update_query, list(data.values()) + [email])
        con.commit()
        print("User updated successfully.")
    except Exception as e:
        print("Error updating user:", e)
    finally:
        if con.is_connected():
            con.close()




def removeUser(email):
    con = mysql.connector.connect(user="root", password="root", database="myusers")
    try:
        cur = con.cursor()
        cur.execute("DELETE FROM User WHERE email = %s", (email,))
        con.commit()
        print("User removed successfully.")
    except Exception as e:
        print("Error removing user:", e)
    finally:
        if con.is_connected():
            con.close()



if len(sys.argv) < 2:
    printHelp()
elif sys.argv[1] == "add":
    addUser(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
elif sys.argv[1] == "ls":
    listUsers(sys.argv[2])
elif sys.argv[1] == "update":
    pass
elif sys.argv[1] == "rm":
    removeUser(sys.argv[2])
else:
    printHelp()
