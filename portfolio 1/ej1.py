import mysql.connector

def crear_tabla_usuarios():
    # Conectarse a la base de datos (asegúrate de tener el servidor MySQL en ejecución)
    conexion = mysql.connector.connect(
        host="localhost",
        user="root",
        password="root",
        database="ejercicios"
    )

    # Crear un cursor para ejecutar comandos SQL
    cursor = conexion.cursor()

    # Crear la tabla de usuarios si no existe
    cursor.execute('''CREATE TABLE IF NOT EXISTS usuarios (
                        id INT AUTO_INCREMENT PRIMARY KEY,
                        nombre VARCHAR(255) NOT NULL,
                        edad INT,
                        email VARCHAR(255) UNIQUE
                    )''')

    # Guardar los cambios y cerrar la conexión
    conexion.commit()
    conexion.close()

# Llamar a la función para crear la tabla de usuarios
crear_tabla_usuarios()

