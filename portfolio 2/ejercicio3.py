import pymongo
import sys
import json

def printHelp():
    print('''
Usage python3 users.py <comand>
Available command:
    - add <name> <surname> <email> <password>
    - ls [<query>]
    - update <email> <att>=<val>(,<att>=<val>)
    - rm <email>
    - addContact <email> <title> <emailContact>
    - lsContact <email>
    - rmContact <email> <emailContact>
''')

def addUser(name, surname, email, password):
    print("addUser()")
    client = pymongo.MongoClient('localhost', 27017)
    try:
        user = {
            "name": name,
            "surname": surname,
            "email": email,
            "password": password,
            "contacts": []
        }
        client['myusers']['users'].insert_one(user)
    finally:
        client.close()

def listUsers(query=""):
    print("listUsers()")
    client = pymongo.MongoClient('localhost', 27017)
    try:
        users = []
        cur = client.myusers.users.find()
        for u in cur:
            users.append(u)
        print(users)
    finally:
        client.close()

def updateUser(email, data={}):
    print("updateUser()")
    client = pymongo.MongoClient('localhost', 27017)
    try:
        query = {
            "email": email
        }
        update_data = {"$set": data}  
        client['myusers']['users'].update_one(query, update_data)
    finally:
        client.close()
    
def removeUser(email):
    print("removeUser()")
    client = pymongo.MongoClient('localhost', 27017)
    try:
        query = {
            "email": email
        }
        client['myusers']['users'].delete_one(query)
    finally:
        client.close()
        
def addContact(email, title, emailContact):
    print("addContact()")
    client = pymongo.MongoClient('localhost', 27017)
    try:
        contact = {
            "title": title,
            "email": emailContact
        }
        client['myusers']['users'].update_one(
            {"email": email},
            {"$push": {"contacts": contact}}
        )
    finally:
        client.close()
        
def listContact(email):
    print("listContact()")
    client = pymongo.MongoClient('localhost', 27017)
    try:
        user = client['myusers']['users'].find_one({"email": email}, {"contacts": 1, "_id": 0})
        if user and "contacts" in user:
            print(user["contacts"])
        else:
            print("No contacts found.")
    finally:
        client.close()

def rmContact(email, emailContact):
    print("rmContact()")
    client = pymongo.MongoClient('localhost', 27017)
    try:
        client['myusers']['users'].update_one(
            {"email": email},
            {"$pull": {"contacts": {"email": emailContact}}}
        )
    finally:
        client.close()

print(sys.argv)
if len(sys.argv) < 2: printHelp()
elif sys.argv[1] == "add":
    addUser(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
elif sys.argv[1] == "ls":
    if len(sys.argv) <= 2: listUsers()
    else: listUsers(sys.argv[2])
elif sys.argv[1] == "update":
    if len(sys.argv) >= 4:
        update_data = {}
        for arg in sys.argv[3:]:
            key_value = arg.split('=')
            if len(key_value) == 2:
                key, value = key_value
                update_data[key.strip()] = value.strip()
        updateUser(sys.argv[2], update_data)
    else:
        print("Falta el correo electrónico y/o los datos para actualizar.")
        printHelp()
elif sys.argv[1] == "rm":
    removeUser(sys.argv[2])
elif sys.argv[1] == "addContact":
    addContact(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == "lsContact":
    listContact(sys.argv[2])
elif sys.argv[1] == "rmContact":
    rmContact(sys.argv[2], sys.argv[3])
else: printHelp()
