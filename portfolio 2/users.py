import redis
import sys
import json

def printHelp():
	print('''
Usage python3 users.py <comand>
Available command:
	- add <name> <surname> <email> <password>
	- ls [<query>]
	- update <email> <att=<val>(,<att>=<val>)
	- rm <email>
''')



def addUser(name, surname, email, password):
	print("addUser()")
	r = redis.Redis(host = 'localhost', port = 6379,db=0)
	try:
		user = {
			"name" : name,
			"surname" : surname,
			"email" : email,
			"password" : password
		}
		data = json.dumps(user)
		r.set(f"/users/{email}" , data)
		print("usuario añadido")
	finally:
		r.close()




def listUsers():
    r = redis.Redis(host='localhost', port=6379, db=0)
    try:
        user_keys = r.keys("/users/*")
        users = []
        for key in user_keys:
            user_data = r.get(key)
            if user_data:
                user = json.loads(user_data)
                users.append(user)
            else:
                print(f"No data found for key: {key}")
        print(users)
    finally:
        r.close()
        
        
        

def updateUser(email, data={}):
    print("updateUser()")
    r = redis.Redis(host='localhost', port=6379, db=0)
    try:
        user_key = f"/users/{email}" 
        existing_data = r.get(user_key)
        if existing_data:
            user = json.loads(existing_data)
            user.update(data)  
            r.set(user_key, json.dumps(user))
            print(f"User updated: {email}")
        else:
            print("El usuario no se ha encontrado")
    except Exception as e:
        print(f"Error: {e}")
    finally:
        r.close()
        
        
        
        
def removeUser(email):
	print("removeUser()")
	r = redis.Redis(host='localhost', port=6379, db=0)
	try:
		user_key = f"/users/{email}"
		if r.exists(user_key):
			r.delete(user_key)
			print("Usuario eliminado correctamente")
		else:
			print("Usuario no encontrado.")
	finally:
		r.close()
		
		
		

	
print(sys.argv)


if len(sys.argv) < 2: printHelp()


elif sys.argv[1] == "add" :
	addUser(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
	
	
	
elif sys.argv[1] == "ls":
	if len(sys.argv) <=2: users=listUsers()
	else: users=listUsers(sys.argv[2])
	
	
	
elif sys.argv[1] == "update":
    if len(sys.argv) >= 4:
        email = sys.argv[2]
        data = {}
        for arg in sys.argv[3:]:
            key_value = arg.split('=')
            if len(key_value) == 2:
                data[key_value[0]] = key_value[1]
            else:
                print("El argumento no se encuentra con el formato 'clave=valor'")
                break
        else:  
            updateUser(email, data)
    else:
        print("Uso: users.py update <email> <attr>=<val>")
        
        
elif sys.argv[1] == "rm":
    removeUser(sys.argv[2])
    
    
else: printHelp()
